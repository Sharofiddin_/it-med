package uz.itmed.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import uz.itmed.entity.Identifier;

@Data
@AllArgsConstructor
public class AppointmentDto {

    private Integer id;
    private Identifier identifier;
    private String patient;
    private String practitioner;
    private String organization;


}
