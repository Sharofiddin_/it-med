package uz.itmed.models;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class AppointmentForm {

    @Pattern(regexp = "[1-9][0-9]{9}", message = "Invalid patient format")
    private String patient;
    @Pattern(regexp = "[1-9][0-9]{9}", message = "Invalid practitioner format")
    private String practitioner;
    @Pattern(regexp = "[1-9][0-9]{9}", message = "Invalid organization format")
    private String organization;
    @Valid
    private IdentifierDto identifier;


}
