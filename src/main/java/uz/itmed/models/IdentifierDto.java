package uz.itmed.models;

import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class IdentifierDto {


    @Pattern(regexp = "[1-9][0-9]*", message = "Invalid format on Identifier.value")
    private String value;
    private String system;


}
