package uz.itmed.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse {
    private Object data;
    private Object error;

    public BaseResponse(Object data, Boolean success) {
        if (success) {
            this.data = data;
        } else {
            this.error = data;
        }
    }
}
