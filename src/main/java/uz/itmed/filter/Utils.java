package uz.itmed.filter;

public class Utils {
    public static Object converter(Class clazz, Object value) throws Exception {
        Object object = null;
        if (clazz == Integer.class) {
            object = Integer.parseInt(value.toString());
        } else if (clazz == String.class) {
            object = value.toString();
        } else {
            object = value;
        }
        return object;
    }
}
