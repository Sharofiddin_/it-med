package uz.itmed.filter;

import lombok.Data;
import uz.itmed.enums.SearchOperator;

@Data
public class SpecSearchCriteria {
    private Object key;
    private SearchOperator searchOperator;
    private Object value;
}
