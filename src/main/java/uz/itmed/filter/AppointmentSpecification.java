package uz.itmed.filter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;
import uz.itmed.entity.Appointment;
import uz.itmed.enums.SearchOperator;

public class AppointmentSpecification implements Specification<Appointment> {

    private List<SpecSearchCriteria> searchCriteriaList;

    public AppointmentSpecification(List<SpecSearchCriteria> searchCriteriaList) {
        this.searchCriteriaList = searchCriteriaList;
    }

    @Override
    public Predicate toPredicate(Root<Appointment> root,
                                 CriteriaQuery<?> criteriaQuery,
                                 CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        for (SpecSearchCriteria searchCriteria : searchCriteriaList) {
            Field field;
            Object value = null;

            try {
                field = Appointment.class.getDeclaredField(searchCriteria.getKey().toString());
                value = Utils.converter(field.getType(), searchCriteria.getValue().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (searchCriteria.getSearchOperator().equals(SearchOperator.EQUAL)) {
                predicates.add(criteriaBuilder.equal(root.get(searchCriteria.getKey().toString()), value));
            }
            if (searchCriteria.getSearchOperator().equals(SearchOperator.MATCHES)) {
                predicates.add(criteriaBuilder.like(root.<String>get(searchCriteria.getKey().toString()), "%" + value.toString() + "%"));
            }
        }
        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
