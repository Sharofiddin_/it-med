package uz.itmed.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import uz.itmed.entity.Appointment;
import uz.itmed.models.AppointmentDto;
import uz.itmed.models.AppointmentForm;

@Mapper(componentModel = "spring", uses = {IdentifierMapper.class})
public interface AppointmentMapper {
    Appointment toEntity(AppointmentForm form);

    @Mapping(target = "id", ignore = true)
    Appointment update(AppointmentForm form, @MappingTarget Appointment entity);

    AppointmentDto toDto(Appointment entity);
}
