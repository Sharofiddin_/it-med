package uz.itmed.mapper;

import org.mapstruct.Mapper;
import uz.itmed.entity.Identifier;
import uz.itmed.models.IdentifierDto;

@Mapper(componentModel = "spring")
public interface IdentifierMapper {
    Identifier toEntity(IdentifierDto dto);
}
