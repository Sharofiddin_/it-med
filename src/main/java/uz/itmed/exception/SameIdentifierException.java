package uz.itmed.exception;

public class SameIdentifierException extends RuntimeException {

    public SameIdentifierException(String message) {
        super(message);
    }
}
