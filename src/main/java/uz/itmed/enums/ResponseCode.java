package uz.itmed.enums;

public enum ResponseCode {

    APPOINTMENT_NOT_FOUND(404 , "Dictionary not found"),
    VALIDATION_ERROR(411, "Validation error"),
    SAME_IDENTIFIER(422 , "Unprocessable Entity");

    private final int code;
    private final String message;

    ResponseCode(int code, String message) {

        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
