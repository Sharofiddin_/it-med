package uz.itmed.service;

import java.util.List;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import uz.itmed.filter.AppointmentSpecification;
import uz.itmed.filter.SpecSearchCriteria;
import uz.itmed.mapper.AppointmentMapper;
import uz.itmed.models.BaseResponse;
import uz.itmed.repository.AppointmentRepository;

@RequiredArgsConstructor
@Service
public class FilterService {
    private final AppointmentRepository repository;
    private final AppointmentMapper mapper;

    @Transactional(readOnly = true)
    public BaseResponse searchUser(List<SpecSearchCriteria> searchCriteria) {
        AppointmentSpecification specification = new AppointmentSpecification(searchCriteria);
        return new BaseResponse(repository.findAll(specification).stream().map(mapper::toDto), true);
    }
}
