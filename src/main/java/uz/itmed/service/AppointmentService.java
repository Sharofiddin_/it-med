package uz.itmed.service;

import java.util.Optional;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.itmed.entity.Appointment;
import uz.itmed.exception.AppointmentNotFoundException;
import uz.itmed.exception.SameIdentifierException;
import uz.itmed.mapper.AppointmentMapper;
import uz.itmed.mapper.IdentifierMapper;
import uz.itmed.models.AppointmentForm;
import uz.itmed.models.BaseResponse;
import uz.itmed.repository.AppointmentRepository;

@Service
@RequiredArgsConstructor
public class AppointmentService {

    private final AppointmentRepository repository;
    private final AppointmentMapper appointmentMapper;
    private final IdentifierMapper identifierMapper;

    public BaseResponse createAppointment(AppointmentForm form) {
        if (repository.existsByIdentifier(identifierMapper.toEntity(form.getIdentifier()))) {
            throw new SameIdentifierException("Already identified by : " + form.getIdentifier());
        }
        return new BaseResponse(appointmentMapper.toDto(repository.save(appointmentMapper.toEntity(form))), true);
    }

    public BaseResponse updateAppointment(Integer id, AppointmentForm form) {
        Optional<Appointment> optional = repository.findById(id);
        if (optional.isEmpty()) {
            throw new AppointmentNotFoundException("Appointment not found with id : " + id);
        }
        return new BaseResponse(appointmentMapper
                .toDto(repository.save(appointmentMapper.update(form, optional.get()))), true);
    }

    public BaseResponse getAppointment(Integer id) {
        Appointment appointment = repository.findById(id).orElseThrow(
                () -> new AppointmentNotFoundException("Appointment not found with id : " + id));
        return new BaseResponse(appointmentMapper.toDto(appointment), true);
    }

    public BaseResponse findAll() {
        var data = repository.findAll();
        if (data.size() == 1) {
            return new BaseResponse(appointmentMapper.toDto(data.get(0)), true);
        }
        if (data.size() == 0) {
            throw new AppointmentNotFoundException("Appointment not found");
        }
        return new BaseResponse(data.stream().map(appointmentMapper::toDto).collect(Collectors.toList()), true);
    }
}
