package uz.itmed.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import uz.itmed.entity.Appointment;
import uz.itmed.entity.Identifier;

public interface AppointmentRepository extends JpaRepository<Appointment, Integer>, JpaSpecificationExecutor<Appointment> {

    boolean existsByIdentifier(Identifier identifier);
}
