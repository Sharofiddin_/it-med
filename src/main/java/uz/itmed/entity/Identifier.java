package uz.itmed.entity;

import javax.persistence.Embeddable;

import lombok.Data;

@Embeddable
@Data
public class Identifier {
    private String value;
    private String system;
}
