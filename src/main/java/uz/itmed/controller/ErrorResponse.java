package uz.itmed.controller;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;
import org.springframework.lang.Nullable;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Value(staticConstructor = "of")
public class ErrorResponse {
    int code;
    String message;
    @Nullable
    Map<String, Object> meta;

    public static ErrorResponse of(int code, String message) {
        return of(code, message, null);
    }
}
