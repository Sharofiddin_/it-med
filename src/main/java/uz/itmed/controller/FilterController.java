package uz.itmed.controller;

import java.util.ArrayList;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import uz.itmed.filter.SpecSearchCriteria;
import uz.itmed.models.BaseResponse;
import uz.itmed.service.FilterService;

@RequiredArgsConstructor
@RestController
public class FilterController {
    private final FilterService service;

    @GetMapping
    public BaseResponse searchUser(@RequestBody ArrayList<SpecSearchCriteria> criteriaArrayList) {
        return service.searchUser(criteriaArrayList);
    }
}
