package uz.itmed.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import uz.itmed.enums.ResponseCode;
import uz.itmed.exception.AppointmentNotFoundException;
import uz.itmed.exception.SameIdentifierException;
import uz.itmed.models.BaseResponse;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(AppointmentNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    BaseResponse on(AppointmentNotFoundException e) {
        return  new BaseResponse(ErrorResponse.of(ResponseCode.APPOINTMENT_NOT_FOUND.getCode(), e.getMessage()),false);
    }

    @ExceptionHandler(SameIdentifierException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    BaseResponse on(SameIdentifierException e) {
        return  new BaseResponse(ErrorResponse.of(ResponseCode.SAME_IDENTIFIER.getCode(), e.getMessage()),false);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    BaseResponse on(MethodArgumentNotValidException e) {
        Map<String, Object> meta = new HashMap<>();
        for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
            meta.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return new BaseResponse(ErrorResponse.of(ResponseCode.VALIDATION_ERROR.getCode(), ResponseCode.VALIDATION_ERROR.getMessage(), meta), false);
    }

}
